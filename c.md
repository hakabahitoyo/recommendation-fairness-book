# Appendix C: Creed of _fair_ user-generated content platforms

There is a hardcore of user generated content platforms which is not too televisionized nor telephonized. _Fair_ means _is not too televisonized_. In the other words,  the users' presence is not oligopolized in a _fair_ user-generated content platform. _Open_ means _is not too telephonized_. The social graph connects various social groups in an _open_ user-generated content platform.

An _user discovery method_ is a method to meet other users in an user-generated content platform. A _fair_ user discovery method prevents the oligopoly of presence of the users, in the other words, supports the decentralization of the users.

A qualitative evaluation of the _fairness_ of an user discovery method is to check following anti-patterns: _oldies effect, positive feedback, power user effect,_ and _shrink-wrap effect_. These anti-patterns bring _unfairness_ to the user discover methods.

_Oldies effect_ is the effect that some users who had been popular in the past still are recommended ever. In a _positive feedbacked_ user discovery method, if some users had beed recommended, those users are further more recommended due to the recommendation itself. _Power user effect_ is the effect that some users who post their content so much tend to be recommended. A _shrink-wrap effected_ user discovery method shows the attributes of the users such as their avatars or names, not contents.

A quantitative evaluation of the _fairness_ of an user discovery method is the geometric mean of the number of followers (or friends) of the top N (N is a constant number) users who are recommended. This quantitative evaluation detects the oldies effect and the positive feedback, not detects the power user effect nor shrink-wrap effect. This is not a serious weakness. There are too many _unfair_ user discovery methods which are oldies effected and positive feedbacked.

## Reference

[1] Creed of _fair_ user-generated content platforms \
https://gitlab.com/hakabahitoyo/creed-of-fair-user-generated-content-platforms

