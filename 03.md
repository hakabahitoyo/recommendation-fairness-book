# Chapter 3: Designing a _fair_ user recommendation system

In this chapter, we design a _fair_ user recommendation system for Mastodon, in step-by-step style.

First of all, we have to avoid the oldies effect. We can choose weekly, monthly, or other criteria with some finite duration.

Second, to avoid the shrink-wrap effect, we have better to use the number of boosts, rather than the numer of followers.

Now we find that, the weekly number of boosts is a _fair_ criteria to recommend the users.

And one more thing, a reverse _unfair_ criteria is _fair._ For example, we can imagine a rule that the user who has more than 100 followers is not recommended. This rule can be combinated with the weekly number of boosts criteria, which brings some additional _fairness._

Now we get a design of a _fair_ user recommendation system for Mastodon. Implementation is expected.
