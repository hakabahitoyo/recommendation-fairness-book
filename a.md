# Appendix A: A case study of qualitative evaluation of the _unfairness_ of the user discovery methods for Mastodon

## User recommendation systems for Mastodon

* Lists of recommended users
    * User Matching for Fedierse (Vaginaplant's)
    * Mastodon Recommended User Search (Cucmberium's)
        * Recommended users
        * Similar users
    * Recommended Followers (Osa's)
    * Potential friendship (Mastodon's official/embedded)
* _Who to follow_ panels
    * Pawoo
    * Pleroma
    * Halcyon
    * Misskey

## Other user discovery methods

* Lists of users
    * By their number of followers
    * By their number of boosts
    * Newcomers
    * Recently posted
    * Manual
    * Public profile endorsements (Mastodon's official/embedded)
* Timelines
    * Home
    * Federated
    * Local
* Search engines

## Summary of fairness and unfairness

* ↑ _Fair_
* Newcomers
* User Matching for Fediverse (Vaginaplant's)
* Similar users (Cucmberium's)
* Search engines
* Potential friendship (Mastodon's official/embedded)
* Local timeline
* Federated timeline
* Home timeline
* Recently posted
* Pawoo's _who to follow_ panel
* Misskey's _who to follow_ panel
* List of users by their number of boosts
* Public profile endorsements (Mastodon's official/embedded)
* Manual guidebooks
* Recommended users (Cucmberium's)
* Recommended Followers (Osa's)
* List of users by their number of followers
* ↓ _Unfair_

## Case 1: List of users by their number of followers

### Implementation

* Mastodon Ranking [4] by User Local, Inc. 

### Specification

* Ordered by the number of followers.

### _Unfairness_

* Oldies effect: Once we had followed an user, we rarely unfollow the one, even if the user becomes not attractive anymore.
* Positive feedback: If we follow the users in this kind of list, it directly accelerates itself.
* Shrink-wrap effect: The list of users shows screen names, bios, and avatars. No content is shown apparently. We are attracted by the famous names and aesthetic avatars.

### _Fairness_

* No power user effect: By definition.

### Summary

* One of the most _unfair_ user discovery methods ever.

## Case 2: Recommended Followers

### Implementation

* Recommended Followers [5] by Osa

### Specification

* When you use this recommendation engine, it shows the users who are followed by the users who you follow.

### _Unfairness_

* The same oldies effect,  positive feedback, and shrink-wrap effect as the list of users by their number of followers.
* Recommends the celebrities in the small society.

### _Fairness_

* No power user effect: By definition.

### Summary

* One of the most _unfair_ user discovery methods ever.

## Case 3: Recommended users in Mastodon Recommended User Search

### Implementation

* Mastodon Recommended User Search [6] by Cucmberium

### Specification

* The users who your similar users follow.

### _Unfairness_

* The same oldies effect,  positive feedback, and shrink-wrap effect as the list of users by their number of followers.

### _Fairness_

* No power user effect: By definition.

### Summary

* One of the most _unfair_ user discovery methods ever.

## Case 4: Manual _guidebooks_

### Implementation

* Mastodon Famous Accounts [7] by Takumi

### Specification

* A blog post which is manually written.

### _Unfairness_

* Oldies effect: Manual guidebooks are rarely updated.
* Shrink-wrap effect: The authors of the guidebooks are attracted by famous names or aesthetic avatars.
* The authors of the guidebooks sometimes just follow some existing authority.

### _Fairness_

* No positive feedback: Our action does not affect to the static documents.
* No power user effect: By definition.

### Summary

* Super _unfair_ user discovery.
* _Fair_ minded authors of the guidebooks are expected.

## Case 5: Public profile endorsements

### Implementation

* Mastodon's officiel/embedded [8]

### Specification

* The user pins other users on one's profile.

### _Fairness_ and _Unfairness_

* Almost same as the manual _guidebooks._

### Summary

* Super _unfair_ user discovery.
* Endorsement is worship or flattery [9].

## Case 6: List of users by their number of boosts

### Implementation

* Mastodon Ranking [4] by User Local, Inc. 

### Specification

* Ordered by the number of boosts.

### _Unfairness_

* Oldies effect: The number of boosts is accumulated ever.
* Positive feedback: If we follow the user who is boosted well, we boost such kind of user's posts more often.
* Power user effect: More posts, more boosts.

### _Fairness_

* No shrink-wrap effect: We read the post before we boost it.

### Summary

* Super _unfair_ user discovery.
* Better than the user discovery methods which depend on the number of followers.

## Case 7: Misskey's _who to follow_ panel

### Implementation

* Misskey [10]

### Specification

* Posting in the last 7 days.
* Local users.
* Many followers.

### _Unfairness_

* The same positive feedback and shrink-wrap effect as the list of users by their number of followers.

### _Fairness_

* Less oldies effect: Requires posting in the last 7 days.
* No power user effect: By definition.

### Summary

* Remarkably _unfair_ user discovery.
* Better than the oldies effected user discovery methods.

## Case 8: Pawoo's _who to follow_ panel

### Implementation

* Pawoo [11, 12]

### Specification

* Following in pixiv.
* Popular (many favorited, many followers, many pictures) users.
* Active (many recent posts) users.

### _Unfairness_
* Many favorited and many followers criteria causes oldies effect, positive feedback, and shrink-wrap effect.
* Many pictures criteria causes oldies effect.
* Many favorited and many pictures criteria cause power user effect.

### _Fairness_

* Less oldies effect: Requires recent posts.

### Summary

* Remarkably _unfair_ user discovery.
* Better than the oldies effected user discovery methods.

## Case 9: Recently posted

### Implementstion

* Profile directory [13] (Mastodon's official/embedded, default)

### Specification

* List of the users who recently posted.

### _Unfairness_

* Power user effect: By definition.
* shrink-wrap effect: By definition.

### _Fairness_

* No oldies effect: By defintion.
* No positive feedback: By definition.

### Summary

* Still _unfair_ user discovery.
* Power user effect remarkably brings unfairness.

## Case 10: Home timeline

### Implementstion

* Embedded

### Specification

* Following users' posts.
* Following users' boosts.

### _Unfairness_

* Positive feedback: If you follow some users and boost their posts, it accelerates itself.
* Power user effect: By definition.

### _Fairness_

* No oldies effect: By definition.
* No shrink-wrap effect: By definition.

### Summary

* Still _unfair_ user discovery.
* Positive feedback remarkably brings unfairness.

## Case 11: Federated timeline

### Implementstion

* Embedded

### Specification

* Local users' posts.
* Posts by the remote users who are followed by the local users.

### _Unfairness_

* Positive feedback: If a local user follows a remote user, the other local users may follow the remote user via their federated timeline.
* Power user effect: By definition.

### _Fairness_

* No oldies effect: By definition.
* No shrink-wrap effect: By definition.

### Summary

* Still _unfair_ user discovery.
* Mixed feature of the home and local timelines.

## Case 12: Local timeline

### Implementstion

* Embedded

### Specification

* Local users' posts

### _Unfairness_

* Power user effect: By definition.

### _Fairness_

* No oldies effect: By definition.
* No positive feedback: By definition.
* No shrink-wrap effect: By definition.

### Summary
* Still unfair user discovery.
* Power user effect remarkably brings unfairness.

## Case 13: Potential friendship

### Implementstion

* Mastodon's official/embedded [14]

### Specification

* When an user engages (replies, favourites, and boosts) other user, the potential friendship counter is bumped.

### _Fairness_ and _Unfairness_

* Mixed feature of the home, fedarated, and local timelines [15].

### Summary

* Still _unfair_ user discovery.

## Case 14: Search engines

### Implementations

* Tootsearch [16]
* Mastodon Realtime Search [17]
* Mastodon Search Portal [18]
* Mastodon Search by Google [19]

### Specification

* Search posts by keyword.

### _Unfairness_

* Power user effect: More posts, more hits.

### _Fairness_

* No oldies effect: By definition.
* No positive feedback: By definition.
* No shrink-wrap effect: By definition.

### Summary

* Still _unfair_ user discovery.
* Not suit for the user discovery use. Explicit keyword is mandatory.

## Case 15: Similar users in Mastodon Recommended User Search

### Implementation

* Mastodon Recommended User Search [6] by Cucmberium

### Specification

* The users who follow who you follow.

### _Unfairness_

* Shrink-wrap effect: By definition.

### _Fairness_

* No oldies effect: By definition.
* No positive feedback: By definition.
* No power user effect: By definition.

### Summary

* Neutral for _fairness_ and _unfairness._
* Small cliques or clusters are formed.

## Case 16: User Matching for Fediverse

### Implementations

* User Matching for Fediverse [20] by Vaginaplant
* _Who to follow_ panel in Pleroma [21]
* _Who to follow_ panel in Halcyon [22]

### Specification

* The users who have the vocabulary which similar to you.
* Vocabulary is abstracted from one's screen name, bio, and posts.
* Users who have too much followers (500 by default) are omitted.

### _Unfairness_

* Shrink-wrap effect: By definition.

### _Fairness_
* No oldies effect: By definition.
* Reverse positive feedback: Users who have too much followers are omitted.
* No power user effect: By definition.

### Summary

* _Fair_ user discovery.

## Case 17: Newcomers

### Implementations

* Profile directory [13] (Mastodon's official/embedded)
* Newcomers in Fediverse [23] by Vaginaplant

### Specification

* List of newcomers.

### _Unfairness_

* Shrink-wrap effect: By definition.

### _Fairness_

* Reverse oldies effect: By definition.
* No positive feedback: By definition.
* No power user effect: By definition.

### Summary

* _Fair_ user discovery.

## References

[1] Hakaba Hitoyo, 脱中央集権のためのデザイン: セレブのためのインターネットを99 %の手に取り戻す,\
​https://web.archive.org/web/20180105170641/https://hakabahitoyo.wordpress.com/


[2] Hakaba Hitoyo, マストドンのユーザーレコメンデーションがブームに？,\
https://hakabahitoyo.wordpress.com/2018/03/29/mastodon-user-recommendation-2018 (Broken link)

​
[3] Hakaba Hitoyo, マストドンのユーザーレコメンデーションについて追記,\
​https://hakabahitoyo.wordpress.com/2018/04/18/misskey-halcyon-recommendation (Broken link)


[4] User Local, Inc., Mastodon Ranking,\
http://mastodon.userlocal.jp (Outdated)

​
[5] Osa, Recommended Followers,\
https://followlink.osa-p.net/recommend.html (Outdated)

​
[6] Cucmberium, Mastodonおすすめユーザー検索,\
https://nocona.cucmber.net/eryngium (Broken link)

​
[7] Takumi, マストドン (Mastodon) ユーザなら必ずフォローしたい！ アカウント一覧,\
https://takulog.info/mastodon-famous-accounts

​
[8] Gargron, Public profile endorsements (accounts picked by profile owner) #8146,\
https://github.com/tootsuite/mastodon/pull/8146

​
[9] Hakaba Hitoyo, Endorsement is some kind of worship or flattery,\
https://github.com/tootsuite/mastodon/pull/8146#issuecomment-411702072

​
[10] https://github.com/syuilo/misskey/blob/master/src/server/api/endpoints/users/recommendation.ts (Broken link)

​
[11] pixiv Inc., Pawoo,\
https://pawoo.net

​
[12] https://github.com/pixiv/mastodon/blob/pawoo/app/models/suggested_account_query.rb

​
[13] Gargron, Add profile directory to web UI #11688,\
https://github.com/tootsuite/mastodon/pull/11688

​
[14] Gargron, Re-add follow recommendations API #7918,\
https://github.com/tootsuite/mastodon/pull/7918

​
[15] Hakaba Hitoyo, ユーザーレコメンデーションがマストドン本体に導入された: フェアネスの評価は「現状維持」,/
https://hakabahitoyo.wordpress.com/2018/07/03/gargrons-official-user-recommendation (Broken link)

​
[16] Kirino Minato, Tootsearch,\
https://tootsearch.chotto.moe

​
[17] User Local, Inc., マストドンリアルタイム検索,\
http://realtime.userlocal.jp (Broken link)

​
[18] マストドン検索ポータル,\
http://mastodonsearch.jp (Outdated)

​
[19] Osa, マストドン検索 by google,\
https://mastosearch.osa-p.net

​
[20] Hakaba Hitoyo, User Matching for Fediverse,\
https://distsn.org/user-match.html (Broken link)

​
[21] lain, Pleroma,\
https://pleroma.social

​
[22] Neetshin, Niklas Poslovski, Halcyon,\
https://www.halcyon.social

​
[23] Hakaba Hitoyo, Newcomers in Fediverse,\
https://distsn.org/user-new.html (Broken link)

​
[24] Hakaba Hitoyo, User Recommendation Systems for Mastodon and their Fairness,\
https://hakabahitoyo.gitlab.io/slides/recommendations (Broken link)
​
